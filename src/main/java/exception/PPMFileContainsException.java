package exception;

public class PPMFileContainsException extends Exception {
    public PPMFileContainsException() {
        super();
    }

    public PPMFileContainsException(String message) {
        super(message);
    }

    public PPMFileContainsException(String message, Throwable cause) {
        super(message, cause);
    }

    public PPMFileContainsException(Throwable cause) {
        super(cause);
    }
}
