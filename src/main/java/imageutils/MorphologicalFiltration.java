package imageutils;

import java.awt.*;
import java.awt.image.BufferedImage;

import static imageutils.filtration.Filtration.copyImage;

public class MorphologicalFiltration {

    public static BufferedImage filterDilatation(BufferedImage image) {
        BufferedImage copyImage = copyImage(image);
        for (int w = 1; w < image.getWidth() - 1; w++) {
            for (int h = 1; h < image.getHeight() - 1; h++) {

                boolean sameColor = true;
                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        Color color = new Color(image.getRGB(w + i, h + j));
                        int red = color.getRed();
                        if (red == 0) {
                            sameColor = false;
                        }
                    }
                }
                Color color;
                if (sameColor) {
                    color = Color.WHITE;
                } else {
                    color = Color.BLACK;
                }
                copyImage.setRGB(w, h, color.getRGB());
            }
        }

        return copyImage;
    }

    public static BufferedImage filterErosion(BufferedImage image) {
        BufferedImage copyImage = copyImage(image);
        for (int w = 1; w < image.getWidth() - 1; w++) {
            for (int h = 1; h < image.getHeight() - 1; h++) {

                boolean sameColor = true;
                int white = 0;
                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        Color color = new Color(image.getRGB(w + i, h + j));
                        int red = color.getRed();
                        if (red == 0) {
                            white++;
                            sameColor = false;
                        }
                    }
                }
                Color color;
                if (sameColor) {
                    color = Color.white;
                } else if (white != 9) {
                    color = Color.white;
                } else {
                    color = Color.black;
                }
                copyImage.setRGB(w, h, color.getRGB());
            }
        }

        return copyImage;
    }

    public static BufferedImage filterOpening(final BufferedImage image) {
        return filterDilatation(filterErosion(image));
    }

    public static BufferedImage filterClosing(final BufferedImage image) {
        return filterErosion(filterDilatation(image));
    }

    public static BufferedImage hitOrMiss(final BufferedImage image, boolean thinning) {
        BufferedImage copyImage = copyImage(image);
        BufferedImage finalImage = copyImage(image);
        for (int w = 1; w < image.getWidth() - 1; w++) {
            for (int h = 1; h < image.getHeight() - 1; h++) {

                boolean sameColor = true;
                int white = 0;
                for (int i = -1; i < 2; i++) {
                    for (int j = -1; j < 2; j++) {
                        if ((i == -1 && j == 0) || (i == 0 && j == 0) || (i == 1 && j == 0) || (i == 0 && j == -1) || (i == 0 && j == 1)) {
                            Color color = new Color(image.getRGB(w + i, h + j));
                            int red = color.getRed();
                            if (thinning) {
                                if (red == 0) {
                                    sameColor = false;
                                    break;
                                }
                            } else {
                                if (red == 255) {
                                    sameColor = false;
                                    break;
                                }
                            }


                        }
                    }
                }
                Color color;
                if (sameColor) {
                    color = Color.white;
                } else {
                    color = Color.black;
                }
                copyImage.setRGB(w, h, color.getRGB());
            }
        }
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                Color colorPixel = new Color(image.getRGB(x, y));
                Color colorAfter = new Color(copyImage.getRGB(x, y));
                int color;
                if (thinning) {
                    color = Math.max(colorPixel.getRed() - colorAfter.getRed(), 0);
                } else {
                    color = Math.min(colorPixel.getRed() + colorAfter.getRed(), 255);

                }
                finalImage.setRGB(x, y, new Color(color, color, color).getRGB());
            }
        }


        return finalImage;
    }
}
