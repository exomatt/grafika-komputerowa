package imageutils;

import lombok.extern.java.Log;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.imageio.stream.ImageOutputStream;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Log
public class ImageReaderSaver {
    public static void saveImage(BufferedImage img, String path, Float quality) throws IOException {
        String format = path.substring(path.lastIndexOf('.') + 1);
        if (format.equals("png")) {
            ImageIO.write(img, "png", new File(path));
            return;
        }
        if (!format.equals("jpg") && !format.equals("jpeg")) {
            JOptionPane.showMessageDialog(null, "File format is not image file (.jpg)!!", "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }
        ImageWriter jpgWriter = ImageIO.getImageWritersByFormatName("jpg").next();
        ImageWriteParam jpgWriteParam = jpgWriter.getDefaultWriteParam();
        jpgWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        jpgWriteParam.setCompressionQuality(quality);

        ImageOutputStream outputStream = new FileImageOutputStream(
                new File(path));
        jpgWriter.setOutput(outputStream);
        IIOImage outputImage = new IIOImage(img, null, null);
        jpgWriter.write(null, outputImage, jpgWriteParam);
        jpgWriter.dispose();
    }

    static private BufferedImage ppmp6(int width, int height, int maxValue, Byte[] data) {
        if (maxValue == 255) {
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            int r = 0;
            int g = 0;
            int b = 0;
            int k = 0;
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    r = data[k++] & 0xFF;
                    g = data[k++] & 0xFF;
                    b = data[k++] & 0xFF;
                    image.setRGB(x, y, new Color(r, g, b).getRGB());
                }
            }

            return image;
        } else {
            //scale
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
            int r = 0;
            int g = 0;
            int b = 0;
            int k = 0;
            for (int y = 0; y < height; y++) {
                for (int x = 0; (x < width) && ((k + 6) < data.length); x++) {
                    r = data[k++] & 0xFF;
                    r = (int) (((double) r / (double) maxValue) * (double) 255);
                    g = data[k++] & 0xFF;
                    g = (int) (((double) g / (double) maxValue) * (double) 255);
                    b = data[k++] & 0xFF;
                    b = (int) (((double) b / (double) maxValue) * (double) 255);
                    image.setRGB(x, y, new Color(r, g, b).getRGB());
                }
            }
            return image;
        }
    }

    public static BufferedImage loadImage(String path) throws Exception {
        BufferedImage image = null;
        File file = new File(path);
        String format = path.substring(path.lastIndexOf('.') + 1);
        if (!format.equals("ppm")) {
            try {
                image = ImageIO.read(new File(path));
            } catch (IOException ex) {
//                log.severe("Error while reading file " + ex.getMessage() + "\n " + Arrays.toString(ex.getStackTrace()));
            }
            return image;
        }

        FileInputStream fileInputStream = new FileInputStream(file);
        BufferedInputStream inputStream = new BufferedInputStream(fileInputStream, 1024);
        char[] fileType = new char[2];
        fileType[0] = (char) inputStream.read();
        fileType[1] = (char) inputStream.read();
        String type = new String(fileType);
//        log.info("Read file type: " + type);
        char c = ' ';
        int width = 0;
        int height = 0;
        int maxValue = 0;
        int i = 0;
        while (true) {
            do if (!Character.isWhitespace(c)) {
                break;
            } while (Character.isWhitespace(c = (char) inputStream.read()));
            if (c == '#') {
                while ((c = (char) inputStream.read()) != '\n') ;
            } else {
                if (i == 3) {
                    break;
                }
                if (!Character.isDigit(c)) {
                    throw new Exception("File contains broken thinks");
                }
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(c);
                while (Character.isDigit(c = (char) inputStream.read()))
                    stringBuilder.append(c);
                switch (i) {
                    case 0:
                        width = Integer.parseInt(stringBuilder.toString());
                        break;
                    case 1:
                        height = Integer.parseInt(stringBuilder.toString());
                        break;
                    case 2:
                        maxValue = Integer.parseInt(stringBuilder.toString());
                        break;
                }
                i++;
            }
        }
        if (type.equals("P3")) {
            image = ppmp3(inputStream, c, width, height, maxValue);
        } else if (type.equals("P6")) {
            image = ppmp6(inputStream, width, height, maxValue, c);
            return image;

        } else {
            log.info("Cannot recognize file type");
            JOptionPane.showMessageDialog(null, "File format is not image file P3 or P6ss!!", "Error", JOptionPane.ERROR_MESSAGE);

        }
        return image;
    }

    private static BufferedImage ppmp6(BufferedInputStream inputStream, int width, int height, int maxValue, char c) throws IOException {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int r = 0;
        int g = 0;
        int b = 0;
        if (maxValue == 255)
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    if (x == 0 && y == 0) {
                        byte[] buffer = new byte[2];
                        inputStream.read(buffer);
                        r = c;
                        g = buffer[0] & 0xFF;
                        b = buffer[1] & 0xFF;
                        image.setRGB(x, y, new Color(r, g, b).getRGB());
                    }
                    byte[] buffer = new byte[3];
                    int rc = inputStream.read(buffer);
                    if (rc == -1) {
                        return image;
                    }
                    r = buffer[0] & 0xFF;
                    g = buffer[1] & 0xFF;
                    b = buffer[2] & 0xFF;
                    image.setRGB(x, y, new Color(r, g, b).getRGB());

                }
            }
        else {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    byte[] buffer = new byte[3];
                    int rc = inputStream.read(buffer);
                    if (rc == -1) {
                        return image;
                    }
                    r = buffer[0] & 0xFF;
                    r = (int) (((double) r / (double) maxValue) * (double) 255);
                    g = buffer[1] & 0xFF;
                    g = (int) (((double) g / (double) maxValue) * (double) 255);
                    b = buffer[1] & 0xFF;
                    b = (int) (((double) b / (double) maxValue) * (double) 255);
                    image.setRGB(x, y, new Color(r, g, b).getRGB());
                }
            }
        }
        return image;
    }

    private static BufferedImage ppmp3(BufferedInputStream inputStream, char c, int width, int height, int maxValue) throws IOException {
        BufferedImage image = image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        int r = 0;
        int g = 0;
        int b = 0;
        if (maxValue == 255) {
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    for (int j = 0; j < 3; j++) {
                        StringBuilder read = new StringBuilder();
                        while (Character.isWhitespace(c)) {
                            c = (char) inputStream.read();
                        }
                        if (c == '#')
                            while ((c = (char) inputStream.read()) != '\n') ;

                        while (Character.isDigit(c)) {
                            read.append(c);
                            c = (char) inputStream.read();
                        }
                        if (read.toString().equals("")) {
                            j--;
                            continue;
                        }
                        int value = 0;
                        try {
                            value = Integer.parseInt(read.toString());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            continue;
                        }
                        if (j % 3 == 0) {
                            r = value;
                        } else if (j % 3 == 1) {
                            g = value;
                        } else {
                            b = value;
                            image.setRGB(x, y, new Color(r, g, b).getRGB());
                        }
                    }
                }
            }
        } else {
            //number X falls between A and B, and you would like Y to fall between C and D
            // Y = (X-A)/(B-A) * (D-C) + C
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    for (int j = 0; j < 3; j++) {
                        StringBuilder read = new StringBuilder();
                        while (Character.isWhitespace(c)) {
                            c = (char) inputStream.read();
                        }
                        if (c == '#')
                            while ((c = (char) inputStream.read()) != '\n') ;

                        while (Character.isDigit(c)) {
                            read.append(c);
                            c = (char) inputStream.read();
                        }
                        if (read.toString().equals("")) {
                            j--;
                            continue;
                        }
                        int value = 0;
                        try {
                            value = Integer.parseInt(read.toString());
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                            continue;
                        }
                        if (j % 3 == 0) {
                            r = value;
                            r = (int) (((double) r / (double) maxValue) * (double) 255);
                        } else if (j % 3 == 1) {
                            g = value;
                            g = (int) (((double) g / (double) maxValue) * (double) 255);
                        } else {
                            b = value;
                            b = (int) (((double) b / (double) maxValue) * (double) 255);
                            image.setRGB(x, y, new Color(r, g, b).getRGB());
                        }
                    }
                }
            }
        }
        return image;
    }
}
