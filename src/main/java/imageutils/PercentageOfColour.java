package imageutils;

import java.awt.*;
import java.awt.image.BufferedImage;

public class PercentageOfColour {

    public static int[] percentage(BufferedImage image) {
//        0- red, 1-yellow, 2-green, 3-cyan, 4-blue, 5-magenta, 6-nearly white, 7-nearly black
        int[] colors = new int[]{0, 0, 0, 0, 0, 0, 0, 0};

        for (int w = 0; w < image.getWidth(); w++) {
            for (int h = 0; h < image.getHeight(); h++) {
                Color colorToCheck = new Color(image.getRGB(w, h));
                float[] hsb = new float[3];
                Color.RGBtoHSB(colorToCheck.getRed(), colorToCheck.getGreen(), colorToCheck.getBlue(), hsb);
                if (hsb[1] < 0.1 && hsb[2] > 0.9)
                    colors[6] += 1;
                else if (hsb[2] < 0.1)
                    colors[7] += 1;
                else {
                    float deg = hsb[0] * 360;
                    if (deg >= 0 && deg < 30)
                        colors[0] += 1;
                    else if (deg >= 30 && deg < 90)
                        colors[1] += 1;
                    else if (deg >= 90 && deg < 150)
                        colors[2] += 1;
                    else if (deg >= 150 && deg < 210)
                        colors[3] += 1;
                    else if (deg >= 210 && deg < 270)
                        colors[4] += 1;
                    else if (deg >= 270 && deg < 330)
                        colors[5] += 1;
                    else colors[0] += 1;
                }

            }
        }

        return colors;
    }

}
