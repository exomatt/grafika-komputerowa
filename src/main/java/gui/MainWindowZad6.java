package gui;

import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

public class MainWindowZad6 {

    private JFrame frame;
    private JTextField textFieldX;
    private JTextField textFieldY;
    private List<Point2D> points = new ArrayList<>();
    private List<Point2D> controlPoints = new ArrayList<>();
    private Point2D selectedPoint;
    private ButtonGroup bg_1;


    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainWindowZad6 window = new MainWindowZad6();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public MainWindowZad6() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1280, 752);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                super.paintComponent(g);
                final Graphics2D g2 = (Graphics2D) g;
                for (Point2D point2D : points) {
                    RenderingHints qualityHints =
                            new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                                    RenderingHints.VALUE_ANTIALIAS_ON);
                    qualityHints.put(RenderingHints.KEY_RENDERING,
                            RenderingHints.VALUE_RENDER_QUALITY);
                    g2.setRenderingHints(qualityHints);

                    int x = (int) point2D.getX();
                    int y = (int) point2D.getY();
                    g2.drawLine(x, y, x, y);
                }
                if (points.size() > 1) {
                    for (int i = 1; i < points.size(); i++) {
                        Point2D first = points.get(i - 1);
                        Point2D second = points.get(i);
                        int x1 = (int) first.getX();
                        int y1 = (int) first.getY();
                        int x2 = (int) second.getX();
                        int y2 = (int) second.getY();
                        g2.drawLine(x1, y1, x2, y2);
                    }
                }
                if (points.size() > 2) {
                    drawCurve(g2);
                }

            }
        };


        panel.setBackground(Color.white);
        panel.setBounds(12, 12, 873, 628);
        frame.getContentPane().add(panel);

        textFieldX = new JTextField();
        textFieldX.setBounds(897, 26, 124, 19);
        frame.getContentPane().add(textFieldX);
        textFieldX.setColumns(10);

        textFieldY = new JTextField();
        textFieldY.setColumns(10);
        textFieldY.setBounds(897, 66, 124, 19);
        frame.getContentPane().add(textFieldY);

        JButton btnAddPoint = new JButton("Add point");
        btnAddPoint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String x = textFieldX.getText();
                String y = textFieldY.getText();
                if (x.isEmpty() || y.isEmpty())
                    return;

                try {
                    int pointX = Integer.parseInt(x);
                    int pointY = Integer.parseInt(y);
                    Point point = new Point(pointX, pointY);
                    points.add(point);
                    panel.repaint();
                } catch (NumberFormatException ex) {
                    System.out.println("Error");
                }
            }
        });
        btnAddPoint.setBounds(1050, 23, 114, 25);
        frame.getContentPane().add(btnAddPoint);

        JButton btnEditPoint = new JButton("Edit point");
        btnEditPoint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String mode = getSelectedButtonText(bg_1);
                if (mode.equals("edit") && selectedPoint != null) {
                    String x = textFieldX.getText();
                    String y = textFieldY.getText();
                    if (x.isEmpty() || y.isEmpty())
                        return;
                    try {
                        int pointX = (int) Double.parseDouble(x);
                        int pointY = (int) Double.parseDouble(y);
                        Point point = new Point(pointX, pointY);
                        int index = points.indexOf(selectedPoint);
                        points.remove(selectedPoint);

                        points.add(index, point);
                        selectedPoint = null;
                        panel.repaint();
                        clearText();
                    } catch (NumberFormatException ex) {
                        System.out.println("Error");
                    }


                }
            }
        });
        btnEditPoint.setBounds(1050, 60, 114, 25);
        frame.getContentPane().add(btnEditPoint);

        JLabel lblX = new JLabel("X");
        lblX.setBounds(896, 12, 66, 15);
        frame.getContentPane().add(lblX);

        JLabel lblY = new JLabel("Y");
        lblY.setBounds(896, 52, 66, 15);
        frame.getContentPane().add(lblY);

        JButton btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                points.clear();
                panel.repaint();
            }
        });
        btnClear.setBounds(897, 235, 114, 25);
        frame.getContentPane().add(btnClear);

        JRadioButton rdbtnDraw = new JRadioButton("draw");
        rdbtnDraw.setSelected(true);
        rdbtnDraw.setBounds(913, 120, 144, 23);
        frame.getContentPane().add(rdbtnDraw);

        JRadioButton rdbtnEdit = new JRadioButton("edit");
        rdbtnEdit.setBounds(913, 147, 144, 23);
        frame.getContentPane().add(rdbtnEdit);

        JLabel lblMode = new JLabel("Mode: ");
        lblMode.setBounds(903, 97, 66, 15);
        frame.getContentPane().add(lblMode);

        bg_1 = new ButtonGroup();
        bg_1.add(rdbtnDraw);
        bg_1.add(rdbtnEdit);

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String mode = getSelectedButtonText(bg_1);
                if (mode.equals("draw")) {
                    Point2D point = e.getPoint();
                    points.add(point);
                    panel.repaint();
                }
                if (mode.equals("edit")) {
                    Point2D point = e.getPoint();
                    selectPoint(point);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                String mode = getSelectedButtonText(bg_1);
                if (mode.equals("edit")) {
                    Point2D point = e.getPoint();
                    selectPoint(point);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                String mode = getSelectedButtonText(bg_1);
                if (mode.equals("edit") && selectedPoint != null) {
                    Point2D point = e.getPoint();
                    int index = points.indexOf(selectedPoint);
                    points.remove(selectedPoint);
                    points.add(index, point);
                    selectedPoint = null;
                    panel.repaint();

                }
            }
        });
    }

    private void clearText() {
        textFieldX.setText("");
        textFieldY.setText("");
    }

    private void selectPoint(Point2D point2D) {
        System.out.println(" Punkt poczatkowy " + point2D.getX() + " " + point2D.getY());
        for (Point2D point : points) {
            System.out.println("Point " + point.getX() + " " + point.getY());
            if (point.distance(point2D) < 5) {
                selectedPoint = point;
                textFieldX.setText(String.valueOf(selectedPoint.getX()));
                textFieldY.setText(String.valueOf(selectedPoint.getY()));
                return;
            }
        }
    }

    private String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements(); ) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }

    private void drawCurve(Graphics2D g2) {
        double x1 = points.get(0).getX();
        double y1 = points.get(0).getY();
        double y2 = 0;
        double x2 = 0;
        for (double t = .025; t <= 1.025; t += .025) {
            //reset x2,y2
            x2 = 0;
            y2 = 0;
            for (int i = 0; i <= points.size() - 1; i++) {
                x2 += points.get(i).getX() * bernstein(t, points.size() - 1, i);
                y2 += points.get(i).getY() * bernstein(t, points.size() - 1, i);
            }
            g2.setColor(Color.red);
            g2.drawLine((int) x1, (int) y1, (int) x2, (int) y2);
            x1 = x2;
            y1 = y2;

        }
    }

    private double bernstein(double t, int exp, int i) {
        return getNewton(exp, i) * Math.pow(1 - t, exp - i) * Math.pow(t, i);
    }

    private int getNewton(int n, int k) {
        if (k < 0) return 0;
        else if (k > n) return 0;
        else return (factorial(n) / (factorial(k) * factorial(n - k)));
    }


    private int factorial(int n) {
        if (n == 0) {
            return 1;
        }
        if (n == 1) {
            return 1;
        } else return n * factorial(n - 1);
    }
}
