package gui;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.awt.*;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.CubicCurve2D;
import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.stream.Collectors;

public class MainWindowZad7 {

    private JFrame frame;
    private JTextField textFieldX;
    private JTextField textFieldY;
    private List<Point2D> points = new ArrayList<>();
    private List<Point2D> tempPoints = new ArrayList<>();
    private List<Polygon> polygons = new ArrayList<>();
    private Point2D selectedPoint;
    private Polygon selectedPolygon;
    private ButtonGroup bg_1;
    private JTextField textFieldVectorX;
    private JTextField textFieldVectorY;
    private JTextField textFieldScale;
    private JTextField textFieldAlfa;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainWindowZad7 window = new MainWindowZad7();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public MainWindowZad7() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1280, 752);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                super.paintComponent(g);
                final Graphics2D g2 = (Graphics2D) g;
                for (Point2D point2D : points) {
                    RenderingHints qualityHints =
                            new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                                    RenderingHints.VALUE_ANTIALIAS_ON);
                    qualityHints.put(RenderingHints.KEY_RENDERING,
                            RenderingHints.VALUE_RENDER_QUALITY);
                    g2.setRenderingHints(qualityHints);

                    int x = (int) point2D.getX();
                    int y = (int) point2D.getY();
//                    g2.drawLine(x, y, x, y);
                    if (selectedPoint != null) {
                        g2.setColor(Color.blue);
                        g2.fillOval(x, y, 10, 10);
                    } else {
                        g2.setColor(Color.black);
                        g2.fillOval(x, y, 10, 10);
                    }
                }
                for (Polygon polygon : polygons) {
                    RenderingHints qualityHints =
                            new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                                    RenderingHints.VALUE_ANTIALIAS_ON);
                    qualityHints.put(RenderingHints.KEY_RENDERING,
                            RenderingHints.VALUE_RENDER_QUALITY);
                    g2.setRenderingHints(qualityHints);
                    if (selectedPolygon == polygon) {
                        g2.setColor(Color.red);
                        g2.drawPolygon(polygon);
                    } else {
                        g2.setColor(Color.black);
                        g2.drawPolygon(polygon);
                    }
                }
                if (points.size() > 1) {
                    for (int i = 1; i < points.size(); i++) {
                        Point2D first = points.get(i - 1);
                        Point2D second = points.get(i);
                        int x1 = (int) first.getX();
                        int y1 = (int) first.getY();
                        int x2 = (int) second.getX();
                        int y2 = (int) second.getY();
                        g2.drawLine(x1, y1, x2, y2);
                    }
                }
            }
        };


        panel.setBackground(Color.white);
        panel.setBounds(12, 12, 873, 628);
        frame.getContentPane().add(panel);

        textFieldX = new JTextField();
        textFieldX.setBounds(897, 26, 124, 19);
        frame.getContentPane().add(textFieldX);
        textFieldX.setColumns(10);

        textFieldY = new JTextField();
        textFieldY.setColumns(10);
        textFieldY.setBounds(897, 66, 124, 19);
        frame.getContentPane().add(textFieldY);

        JButton btnAddPoint = new JButton("Add point");
        btnAddPoint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String x = textFieldX.getText();
                String y = textFieldY.getText();
                if (x.isEmpty() || y.isEmpty())
                    return;

                try {
                    int pointX = Integer.parseInt(x);
                    int pointY = Integer.parseInt(y);
                    Point point = new Point(pointX, pointY);
                    points.add(point);
                    panel.repaint();
                } catch (NumberFormatException ex) {
                    System.out.println("Error");
                }
            }
        });
        btnAddPoint.setBounds(1050, 23, 114, 25);
        frame.getContentPane().add(btnAddPoint);

        JButton btnEditPoint = new JButton("Edit point");
        btnEditPoint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String mode = getSelectedButtonText(bg_1);
                if (mode.equals("edit") && selectedPoint != null) {
                    String x = textFieldX.getText();
                    String y = textFieldY.getText();
                    if (x.isEmpty() || y.isEmpty())
                        return;
                    try {
                        int pointX = (int) Double.parseDouble(x);
                        int pointY = (int) Double.parseDouble(y);
                        Point point = new Point(pointX, pointY);
                        int index = points.indexOf(selectedPoint);
                        points.remove(selectedPoint);

                        points.add(index, point);
                        selectedPoint = null;
                        panel.repaint();
                        clearText();
                    } catch (NumberFormatException ex) {
                        System.out.println("Error" + ex);
                    }


                }
            }
        });
        btnEditPoint.setBounds(1050, 60, 114, 25);
        frame.getContentPane().add(btnEditPoint);

        JLabel lblX = new JLabel("X");
        lblX.setBounds(896, 12, 66, 15);
        frame.getContentPane().add(lblX);

        JLabel lblY = new JLabel("Y");
        lblY.setBounds(896, 52, 66, 15);
        frame.getContentPane().add(lblY);

        JButton btnClear = new JButton("Clear");
        btnClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                points.clear();
                polygons.clear();
                panel.repaint();
            }
        });
        btnClear.setBounds(1050, 173, 114, 25);
        frame.getContentPane().add(btnClear);

        JRadioButton rdbtnDraw = new JRadioButton("draw");
        rdbtnDraw.setSelected(true);
        rdbtnDraw.setBounds(913, 120, 144, 23);
        frame.getContentPane().add(rdbtnDraw);

        JRadioButton rdbtnEdit = new JRadioButton("edit");
        rdbtnEdit.setBounds(913, 147, 144, 23);
        frame.getContentPane().add(rdbtnEdit);

        JRadioButton rdbtnSelect = new JRadioButton("select");
        rdbtnSelect.setBounds(913, 174, 144, 23);
        frame.getContentPane().add(rdbtnSelect);

        JRadioButton rdbtnMove = new JRadioButton("move");
        rdbtnMove.setBounds(913, 204, 144, 23);
        frame.getContentPane().add(rdbtnMove);

        JLabel lblMode = new JLabel("Mode: ");
        lblMode.setBounds(903, 97, 66, 15);
        frame.getContentPane().add(lblMode);

        JRadioButton rdbtnResize = new JRadioButton("resize");
        rdbtnResize.setBounds(913, 231, 144, 23);
        frame.getContentPane().add(rdbtnResize);

        JRadioButton rdbtnRotate = new JRadioButton("rotate");
        rdbtnRotate.setBounds(913, 259, 144, 23);
        frame.getContentPane().add(rdbtnRotate);

        bg_1 = new ButtonGroup();
        bg_1.add(rdbtnDraw);
        bg_1.add(rdbtnEdit);
        bg_1.add(rdbtnMove);
        bg_1.add(rdbtnSelect);
        bg_1.add(rdbtnRotate);
        bg_1.add(rdbtnResize);

        JButton btnCreateShape = new JButton("Create shape");
        btnCreateShape.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int[] x = new int[points.size()];
                int[] y = new int[points.size()];
                for (int i = 0; i < points.size(); i++) {
                    x[i] = (int) points.get(i).getX();
                    y[i] = (int) points.get(i).getY();
                }
                Polygon polygon = new Polygon(x, y, points.size());
                polygons.add(polygon);
                points.clear();
                panel.repaint();
            }
        });
        btnCreateShape.setBounds(1050, 92, 144, 25);
        frame.getContentPane().add(btnCreateShape);

        JButton btnMoveVector = new JButton("Move vector");
        btnMoveVector.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (selectedPolygon != null) {
                    String x = textFieldVectorX.getText();
                    String y = textFieldVectorY.getText();
                    if (x.isEmpty() || y.isEmpty())
                        return;
                    try {
                        int tempX = (int) Double.parseDouble(x);
                        int tempY = (int) Double.parseDouble(y);
                        polygons.remove(selectedPolygon);
                        int[] xpoints = selectedPolygon.xpoints;
                        int[] ypoints = selectedPolygon.ypoints;
                        int npoints = selectedPolygon.npoints;
                        for (int i = 0; i < npoints; i++) {
                            xpoints[i] += tempX;
                            ypoints[i] += tempY;
                        }
                        Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                        polygons.add(polygon);
                        tempPoints.clear();
                        panel.repaint();
                        selectedPolygon = null;
                        panel.repaint();
                        clearText();
                    } catch (NumberFormatException ex) {
                        System.out.println("Error" + ex);
                    }
                }
            }
        });
        btnMoveVector.setBounds(1050, 326, 144, 25);
        frame.getContentPane().add(btnMoveVector);

        JButton btnScale = new JButton("Scale ");
        btnScale.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (selectedPoint != null) {
                    String k = textFieldScale.getText();
                    if (k.isEmpty()) {
                        return;
                    }
                    try {
                        double tempK = Double.parseDouble(k);
                        double x = selectedPoint.getX();
                        double y = selectedPoint.getY();
                        polygons.remove(selectedPolygon);
                        int[] xpoints = selectedPolygon.xpoints;
                        int[] ypoints = selectedPolygon.ypoints;
                        int npoints = selectedPolygon.npoints;
                        for (int i = 0; i < npoints; i++) {
                            xpoints[i] = (int) (xpoints[i] * tempK + (1 - tempK) * x);
                            ypoints[i] = (int) (ypoints[i] * tempK + (1 - tempK) * y);
                        }
                        Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                        polygons.add(polygon);
                        tempPoints.clear();
                        panel.repaint();
                        selectedPolygon = null;
                        panel.repaint();
                        clearText();

                    } catch (NumberFormatException ex) {
                        System.out.println("Error" + ex);
                    }
                } else {
                    String x = textFieldX.getText();
                    String y = textFieldY.getText();
                    String k = textFieldScale.getText();
                    if (x.isEmpty() || y.isEmpty()) {
                        if (k.isEmpty())
                            return;
                        double tempK = Double.parseDouble(k);
                        polygons.remove(selectedPolygon);
                        int[] xpoints = selectedPolygon.xpoints;
                        int[] ypoints = selectedPolygon.ypoints;
                        int npoints = selectedPolygon.npoints;
                        for (int i = 0; i < npoints; i++) {
                            xpoints[i] = (int) (xpoints[i] * tempK);
                            ypoints[i] = (int) (ypoints[i] * tempK);
                        }
                        Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                        polygons.add(polygon);
                        tempPoints.clear();
                        panel.repaint();
                        selectedPolygon = null;
                        panel.repaint();
                        clearText();
                    } else {
                        try {
                            double tempK = Double.parseDouble(k);
                            double tempX = Double.parseDouble(x);
                            double tempY = Double.parseDouble(y);
                            polygons.remove(selectedPolygon);
                            int[] xpoints = selectedPolygon.xpoints;
                            int[] ypoints = selectedPolygon.ypoints;
                            int npoints = selectedPolygon.npoints;
                            for (int i = 0; i < npoints; i++) {
                                xpoints[i] = (int) (xpoints[i] * tempK + (1 - tempK) * tempX);
                                ypoints[i] = (int) (ypoints[i] * tempK + (1 - tempK) * tempY);
                            }
                            Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                            polygons.add(polygon);
                            tempPoints.clear();
                            panel.repaint();
                            selectedPolygon = null;
                            panel.repaint();
                            clearText();

                        } catch (NumberFormatException ex) {
                            System.out.println("Error" + ex);
                        }

                    }
                }
            }
        });
        btnScale.setBounds(1050, 458, 114, 25);
        frame.getContentPane().add(btnScale);

        JLabel lblPoint = new JLabel("Point");
        lblPoint.setBounds(913, -1, 66, 15);
        frame.getContentPane().add(lblPoint);

        textFieldVectorX = new JTextField();
        textFieldVectorX.setBounds(897, 326, 124, 19);
        frame.getContentPane().add(textFieldVectorX);
        textFieldVectorX.setColumns(10);

        textFieldVectorY = new JTextField();
        textFieldVectorY.setColumns(10);
        textFieldVectorY.setBounds(897, 366, 124, 19);
        frame.getContentPane().add(textFieldVectorY);

        JLabel lblVector = new JLabel("Vector");
        lblVector.setBounds(897, 290, 66, 15);
        frame.getContentPane().add(lblVector);

        JLabel label = new JLabel("X");
        label.setBounds(896, 304, 66, 15);
        frame.getContentPane().add(label);

        JLabel label_1 = new JLabel("Y");
        label_1.setBounds(896, 344, 66, 15);
        frame.getContentPane().add(label_1);

        textFieldScale = new JTextField();
        textFieldScale.setBounds(897, 461, 124, 19);
        frame.getContentPane().add(textFieldScale);
        textFieldScale.setColumns(10);

        JLabel lblScaleParameter = new JLabel("Scale parameter");
        lblScaleParameter.setBounds(897, 434, 144, 15);
        frame.getContentPane().add(lblScaleParameter);

        JButton btnRotation = new JButton("Rotation");
        btnRotation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (selectedPoint != null) {
                    String a = textFieldAlfa.getText();
                    if (a.isEmpty()) {
                        return;
                    }
                    try {
                        double tempAlfa = Double.parseDouble(a);
                        double x = selectedPoint.getX();
                        double y = selectedPoint.getY();
                        polygons.remove(selectedPolygon);
                        int[] xpoints = selectedPolygon.xpoints;
                        int[] ypoints = selectedPolygon.ypoints;
                        int npoints = selectedPolygon.npoints;
                        for (int i = 0; i < npoints; i++) {
                            xpoints[i] = (int) (x + ((xpoints[i] - x) * Math.cos(Math.toRadians(tempAlfa))) - ((ypoints[i] - y) * Math.sin(Math.toRadians(tempAlfa))));
                            ypoints[i] = (int) (y + ((xpoints[i] - x) * Math.sin(Math.toRadians(tempAlfa))) + ((ypoints[i] - y) * Math.cos(Math.toRadians(tempAlfa))));
                        }
                        Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                        polygons.add(polygon);
                        tempPoints.clear();
                        selectedPolygon = null;
                        panel.repaint();
                        clearText();

                    } catch (NumberFormatException ex) {
                        System.out.println("Error" + ex);
                    }
                } else {
                    String x = textFieldX.getText();
                    String y = textFieldY.getText();
                    String a = textFieldAlfa.getText();
                    if (x.isEmpty() || y.isEmpty()) {
                        if (a.isEmpty())
                            return;
                        double tempAlfa = Double.parseDouble(a);
                        polygons.remove(selectedPolygon);
                        int[] xpoints = selectedPolygon.xpoints;
                        int[] ypoints = selectedPolygon.ypoints;
                        int npoints = selectedPolygon.npoints;
                        for (int i = 0; i < npoints; i++) {
                            xpoints[i] = (int) ((xpoints[i] * Math.cos(Math.toRadians(tempAlfa))) - (ypoints[i] * Math.sin(Math.toRadians(tempAlfa))));
                            ypoints[i] = (int) ((xpoints[i] * Math.sin(Math.toRadians(tempAlfa))) + (ypoints[i] * Math.cos(Math.toRadians(tempAlfa))));
                        }
                        Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                        polygons.add(polygon);
                        tempPoints.clear();
                        panel.repaint();
                        selectedPolygon = null;
                        panel.repaint();
                        clearText();
                    } else {
                        try {
                            double tempAlfa = Double.parseDouble(a);
                            double tempX = Double.parseDouble(x);
                            double tempY = Double.parseDouble(y);
                            polygons.remove(selectedPolygon);
                            int[] xpoints = selectedPolygon.xpoints;
                            int[] ypoints = selectedPolygon.ypoints;
                            int npoints = selectedPolygon.npoints;
                            for (int i = 0; i < npoints; i++) {
                                xpoints[i] = (int) (tempX + ((xpoints[i] - tempX) * Math.cos(Math.toRadians(tempAlfa))) - ((ypoints[i] - tempY) * Math.sin(Math.toRadians(tempAlfa))));
                                ypoints[i] = (int) (tempY + ((xpoints[i] - tempX) * Math.sin(Math.toRadians(tempAlfa))) + ((ypoints[i] - tempY) * Math.cos(Math.toRadians(tempAlfa))));
                            }
                            Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                            polygons.add(polygon);
                            tempPoints.clear();
                            panel.repaint();
                            selectedPolygon = null;
                            panel.repaint();
                            clearText();

                        } catch (NumberFormatException ex) {
                            System.out.println("Error" + ex);
                        }

                    }
                }
            }
        });
        btnRotation.setBounds(1050, 523, 114, 25);
        frame.getContentPane().add(btnRotation);

        textFieldAlfa = new JTextField();
        textFieldAlfa.setBounds(897, 526, 124, 19);
        frame.getContentPane().add(textFieldAlfa);
        textFieldAlfa.setColumns(10);

        JLabel lblAlfa = new JLabel("Alfa");
        lblAlfa.setBounds(896, 499, 66, 15);
        frame.getContentPane().add(lblAlfa);

        JButton btnSave = new JButton("save");
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Type listType = new TypeToken<List<Polygon>>(){}.getType();
                Gson gson = new Gson();
                String json = gson.toJson(polygons, listType);
                try {
                    Files.write(Paths.get("./polygons.json"), Collections.singleton(json));
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        });
        btnSave.setBounds(954, 593, 114, 25);
        frame.getContentPane().add(btnSave);

        JButton btnLoad = new JButton("load");
        btnLoad.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Type listType = new TypeToken<List<Polygon>>(){}.getType();
                Gson gson = new Gson();

                try {
                    String json = new String(Files.readAllBytes(Paths.get("./polygons.json")));
                    polygons = gson.fromJson(json, listType);
                    panel.repaint();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }

            }
        });
        btnLoad.setBounds(954, 630, 114, 25);
        frame.getContentPane().add(btnLoad);


        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String mode = getSelectedButtonText(bg_1);
                if (mode.equals("draw")) {
                    Point2D point = e.getPoint();
                    points.add(point);
                    panel.repaint();
                }
                if (mode.equals("edit")) {
                    Point2D point = e.getPoint();
                    selectPoint(point);
                    panel.repaint();
                }
                if (mode.equals("select")) {
                    Point2D point = e.getPoint();
//                    selectedPolygon = null;
//                    selectedPoint = null;
                    selectPoint(point);
                    selectPolygon(point);
                    panel.repaint();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                String mode = getSelectedButtonText(bg_1);
                if (mode.equals("edit")) {
                    Point2D point = e.getPoint();
                    selectPoint(point);
                    panel.repaint();
                }
                if (mode.equals("move")) {
                    Point point = e.getPoint();
                    selectPolygon(point);
                    if (selectedPolygon != null)
                        tempPoints.add(point);
                    panel.repaint();
                }
                if (mode.equals("resize") || mode.equals("rotate")) {
                    Point2D point = e.getPoint();
                    selectPolygon(point);
                    if (selectedPolygon != null)
                        tempPoints.add(point);
                    panel.repaint();
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                String mode = getSelectedButtonText(bg_1);
                if (mode.equals("edit") && selectedPoint != null) {
                    Point2D point = e.getPoint();
                    int index = points.indexOf(selectedPoint);
                    points.remove(selectedPoint);
                    points.add(index, point);
                    selectedPoint = null;
                    panel.repaint();
                }
                if (mode.equals("move") && selectedPolygon != null) {
                    Point2D point2D = tempPoints.get(0);
                    Point2D point2DSecond = e.getPoint();
                    double tempX = point2DSecond.getX() - point2D.getX();
                    double tempY = point2DSecond.getY() - point2D.getY();
                    polygons.remove(selectedPolygon);
                    int[] xpoints = selectedPolygon.xpoints;
                    int[] ypoints = selectedPolygon.ypoints;
                    int npoints = selectedPolygon.npoints;
                    for (int i = 0; i < npoints; i++) {
                        xpoints[i] = (int) (xpoints[i] + tempX);
                        ypoints[i] = (int) (ypoints[i] + tempY);
                    }
                    Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                    polygons.add(polygon);
                    tempPoints.clear();
                    panel.repaint();
                    selectedPolygon = null;
                }
                if (mode.equals("resize")) {
                    Point2D pointClicked = tempPoints.get(0);
                    Point2D pointReleased = e.getPoint();
                    double tempK;
                    if (pointClicked.getX() > pointReleased.getX()) {
                        tempK = 0.5;
                    } else {
                        tempK = 1.5;
                    }
                    if (selectedPoint != null) {
                        try {
                            double x = selectedPoint.getX();
                            double y = selectedPoint.getY();
                            polygons.remove(selectedPolygon);
                            int[] xpoints = selectedPolygon.xpoints;
                            int[] ypoints = selectedPolygon.ypoints;
                            int npoints = selectedPolygon.npoints;
                            for (int i = 0; i < npoints; i++) {
                                xpoints[i] = (int) (xpoints[i] * tempK + (1 - tempK) * x);
                                ypoints[i] = (int) (ypoints[i] * tempK + (1 - tempK) * y);
                            }
                            Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                            polygons.add(polygon);
                            tempPoints.clear();
                            panel.repaint();
                            selectedPolygon = null;
                            panel.repaint();
                            clearText();

                        } catch (NumberFormatException ex) {
                            System.out.println("Error" + ex);
                        }
                    } else {
                        String x = textFieldX.getText();
                        String y = textFieldY.getText();
                        if (x.isEmpty() || y.isEmpty()) {
                            polygons.remove(selectedPolygon);
                            int[] xpoints = selectedPolygon.xpoints;
                            int[] ypoints = selectedPolygon.ypoints;
                            int npoints = selectedPolygon.npoints;
                            for (int i = 0; i < npoints; i++) {
                                xpoints[i] = (int) (xpoints[i] * tempK);
                                ypoints[i] = (int) (ypoints[i] * tempK);
                            }
                            Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                            polygons.add(polygon);
                            tempPoints.clear();
                            panel.repaint();
                            selectedPolygon = null;
                            panel.repaint();
                            clearText();
                        } else {
                            try {
                                double tempX = Double.parseDouble(x);
                                double tempY = Double.parseDouble(y);
                                polygons.remove(selectedPolygon);
                                int[] xpoints = selectedPolygon.xpoints;
                                int[] ypoints = selectedPolygon.ypoints;
                                int npoints = selectedPolygon.npoints;
                                for (int i = 0; i < npoints; i++) {
                                    xpoints[i] = (int) (xpoints[i] * tempK + (1 - tempK) * tempX);
                                    ypoints[i] = (int) (ypoints[i] * tempK + (1 - tempK) * tempY);
                                }
                                Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                                polygons.add(polygon);
                                tempPoints.clear();
                                panel.repaint();
                                selectedPolygon = null;
                                panel.repaint();
                                clearText();

                            } catch (NumberFormatException ex) {
                                System.out.println("Error" + ex);
                            }

                        }
                    }
                }
                if (mode.equals("rotate")) {
                    Point2D pointClicked = tempPoints.get(0);
                    Point2D pointReleased = e.getPoint();
                    double tempAlfa;
                    if (pointClicked.getX() > pointReleased.getX()) {
                        tempAlfa = 10;
                    } else {
                        tempAlfa = -10;
                    }

                    if (selectedPoint != null) {
                        try {
                            double x = selectedPoint.getX();
                            double y = selectedPoint.getY();
                            polygons.remove(selectedPolygon);
                            int[] xpoints = selectedPolygon.xpoints;
                            int[] ypoints = selectedPolygon.ypoints;
                            int npoints = selectedPolygon.npoints;
                            for (int i = 0; i < npoints; i++) {
                                xpoints[i] = (int) (x + ((xpoints[i] - x) * Math.cos(Math.toRadians(tempAlfa))) - ((ypoints[i] - y) * Math.sin(Math.toRadians(tempAlfa))));
                                ypoints[i] = (int) (y + ((xpoints[i] - x) * Math.sin(Math.toRadians(tempAlfa))) + ((ypoints[i] - y) * Math.cos(Math.toRadians(tempAlfa))));
                            }
                            Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                            polygons.add(polygon);
                            tempPoints.clear();
                            selectedPolygon = null;
                            panel.repaint();
                            clearText();

                        } catch (NumberFormatException ex) {
                            System.out.println("Error" + ex);
                        }
                    } else {
                        String x = textFieldX.getText();
                        String y = textFieldY.getText();
                        String a = textFieldAlfa.getText();
                        if (x.isEmpty() || y.isEmpty()) {
                            polygons.remove(selectedPolygon);
                            int[] xpoints = selectedPolygon.xpoints;
                            int[] ypoints = selectedPolygon.ypoints;
                            int npoints = selectedPolygon.npoints;
                            for (int i = 0; i < npoints; i++) {
                                xpoints[i] = (int) ((xpoints[i] * Math.cos(Math.toRadians(tempAlfa))) - (ypoints[i] * Math.sin(Math.toRadians(tempAlfa))));
                                ypoints[i] = (int) ((xpoints[i] * Math.sin(Math.toRadians(tempAlfa))) + (ypoints[i] * Math.cos(Math.toRadians(tempAlfa))));
                            }
                            Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                            polygons.add(polygon);
                            tempPoints.clear();
                            panel.repaint();
                            selectedPolygon = null;
                            panel.repaint();
                            clearText();
                        } else {
                            try {
                                double tempX = Double.parseDouble(x);
                                double tempY = Double.parseDouble(y);
                                polygons.remove(selectedPolygon);
                                int[] xpoints = selectedPolygon.xpoints;
                                int[] ypoints = selectedPolygon.ypoints;
                                int npoints = selectedPolygon.npoints;
                                for (int i = 0; i < npoints; i++) {
                                    xpoints[i] = (int) (tempX + ((xpoints[i] - tempX) * Math.cos(Math.toRadians(tempAlfa))) - ((ypoints[i] - tempY) * Math.sin(Math.toRadians(tempAlfa))));
                                    ypoints[i] = (int) (tempY + ((xpoints[i] - tempX) * Math.sin(Math.toRadians(tempAlfa))) + ((ypoints[i] - tempY) * Math.cos(Math.toRadians(tempAlfa))));
                                }
                                Polygon polygon = new Polygon(xpoints, ypoints, npoints);
                                polygons.add(polygon);
                                tempPoints.clear();
                                panel.repaint();
                                selectedPolygon = null;
                                panel.repaint();
                                clearText();

                            } catch (NumberFormatException ex) {
                                System.out.println("Error" + ex);
                            }

                        }
                    }
                }
            }
        });
    }

    private void selectPolygon(Point2D point) {
        for (Polygon polygon : polygons) {
            if (polygon.contains(point)) {
                selectedPolygon = polygon;
                break;
            }
        }
    }

    private void clearText() {
        textFieldX.setText("");
        textFieldY.setText("");
        textFieldVectorX.setText("");
        textFieldVectorY.setText("");
        textFieldScale.setText("");
        textFieldAlfa.setText("");
    }

    private void selectPoint(Point2D point2D) {
        System.out.println(" Punkt poczatkowy " + point2D.getX() + " " + point2D.getY());
        for (Point2D point : points) {
            System.out.println("Point " + point.getX() + " " + point.getY());
            if (point.distance(point2D) < 10) {
                selectedPoint = point;
                textFieldX.setText(String.valueOf(selectedPoint.getX()));
                textFieldY.setText(String.valueOf(selectedPoint.getY()));
                return;
            }
        }
    }

    private String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements(); ) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }
}
