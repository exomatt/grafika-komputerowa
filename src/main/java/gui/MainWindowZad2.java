package gui;

import imageutils.ImageReaderSaver;
import lombok.extern.java.Log;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

@Log
public class MainWindowZad2 {

    private JFrame frame;
    private BufferedImage image;


    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                MainWindowZad2 window = new MainWindowZad2();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the application.
     */
    public MainWindowZad2() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 854, 559);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        JMenu mnFiles = new JMenu("Files");
        menuBar.add(mnFiles);
        JMenuItem mntmLoad = new JMenuItem("Load");
        mnFiles.add(mntmLoad);
        ImagePanel imagePanel = new ImagePanel(image);
        frame.add(imagePanel);
        mntmLoad.addActionListener(e -> {
            JFileChooser imageOpener = new JFileChooser("/home/exomat/Pulpit/grafika/ppm-obrazy-testowe");
            imageOpener.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    String fileName = f.getName().toLowerCase();
                    return fileName.endsWith(".jpg") || fileName.endsWith(".ppm")
                            || fileName.endsWith(".jpeg");
                }

                @Override
                public String getDescription() {
                    return "Image files (.ppm, .jpeg, .jpg)";
                }
            });

            int returnValue = imageOpener.showDialog(null, "Select image");
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                try {
                    image = ImageReaderSaver.loadImage(imageOpener.getSelectedFile().getPath());
                } catch (Exception ex) {
                    log.severe("Problem with reading image" + ex.getMessage() + Arrays.toString(ex.getStackTrace()));
                    JOptionPane.showMessageDialog(null, "Problem with reading file!!", "Error", JOptionPane.ERROR_MESSAGE);
                }
                imagePanel.setBufferedImage(image);
                frame.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
                imagePanel.repaint();

            }

        });

        JMenuItem mntmSave = new JMenuItem("Save");
        mnFiles.add(mntmSave);
        mntmSave.addActionListener(e -> {
            JFileChooser imageOpener = new JFileChooser("/home/exomat/Pulpit/grafika/ppm-obrazy-testowe");
            imageOpener.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            imageOpener.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    String fileName = f.getName().toLowerCase();
                    return fileName.endsWith(".jpg") || fileName.endsWith(".jpeg");
                }

                @Override
                public String getDescription() {
                    return "Image files (.jpg, .jpeg)";
                }
            });
            int returnValue = imageOpener.showDialog(null, "Save image");
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                String path = imageOpener.getSelectedFile().getPath();
                String inputDialog = JOptionPane.showInputDialog("Input proccent of compression (0 maximum, 100 none )");
                int parseInt;
                try {
                    parseInt = Integer.parseInt(inputDialog);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Please fill with number!!", "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                Float quality = (float) parseInt / 100;
                try {
                    ImageReaderSaver.saveImage(image, path, quality);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
