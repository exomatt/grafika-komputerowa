package gui;

import lombok.extern.java.Log;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

@Log
public class MainWindow {

    public static final String RECTANGLE = "rectangle";
    public static final String ELLIPSE = "ellipse";
    public static final String LINE = "line";
    public static final String RESIZE = "resize";
    public static final String MOVE = "move";
    public static final String DRAW = "draw";
    private JFrame frame;
    private JTextField textFieldX1;
    private JTextField textFieldY1;
    private JTextField textFieldX2;
    private JTextField textFieldY2;
    private List<Shape> shapes = new ArrayList<>();
    private List<Point2D> points = new ArrayList<>();
    private final ButtonGroup bg_2 = new ButtonGroup();
    private Shape selectedShape;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                MainWindow window = new MainWindow();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the application.
     */
    public MainWindow() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1184, 738);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        final JPanel panel = new JPanel() {
            @Override
            public void paint(Graphics g) {
                super.paint(g);
                super.paintComponent(g);
                final Graphics2D g2 = (Graphics2D) g;
                if (shapes != null) {
                    for (Shape shape : shapes) {
                        RenderingHints qualityHints =
                                new RenderingHints(RenderingHints.KEY_ANTIALIASING,
                                        RenderingHints.VALUE_ANTIALIAS_ON);
                        qualityHints.put(RenderingHints.KEY_RENDERING,
                                RenderingHints.VALUE_RENDER_QUALITY);
                        g2.setRenderingHints(qualityHints);
                        g2.draw(shape);
                    }
                }
            }
        };
        panel.setBounds(22, 39, 909, 653);
        frame.getContentPane().add(panel);

        JLabel lblShape = new JLabel("Shape:");
        lblShape.setBounds(12, 12, 66, 15);
        frame.getContentPane().add(lblShape);

        JRadioButton rdrectangle = new JRadioButton(RECTANGLE);
        rdrectangle.setBounds(92, 8, 95, 23);
        frame.getContentPane().add(rdrectangle);

        JRadioButton rdellipse = new JRadioButton(ELLIPSE);
        rdellipse.setBounds(195, 8, 85, 23);
        frame.getContentPane().add(rdellipse);

        JRadioButton rdline = new JRadioButton(LINE);
        rdline.setBounds(300, 8, 72, 23);
        rdline.setSelected(true);
        frame.getContentPane().add(rdline);
        final ButtonGroup bg_1 = new ButtonGroup();
        bg_1.add(rdrectangle);
        bg_1.add(rdellipse);
        bg_1.add(rdline);

        panel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                String mode = getSelectedButtonText(bg_2);
                if (mode.equals(RESIZE)) {
                    Point point = e.getPoint();
                    selectShape(point);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                String mode = getSelectedButtonText(bg_2);
                if (mode.equals(DRAW)) {
                    points.add(e.getPoint());
                } else if (mode.equals(MOVE) || mode.equals(RESIZE)) {
                    Point point = e.getPoint();
                    points.add(point);
                    selectShape(point);
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                String mode = getSelectedButtonText(bg_2);
                if (mode.equals(DRAW)) {
                    String text = getSelectedButtonText(bg_1);
                    log.info("Draw: " + text);
                    Point2D point2D = points.get(0);
                    Point2D point2DSecond = e.getPoint();
                    double w = point2DSecond.getX() - point2D.getX();
                    double h = point2DSecond.getY() - point2D.getY();
                    if (w < 0) {
                        w = w * -1;
                    }
                    if (h < 0) {
                        h = h * -1;
                    }
                    switch (text) {
                        case LINE: {
                            final Line2D temp = new Line2D.Double(point2D.getX(), point2D.getY(),
                                    point2DSecond.getX(), point2DSecond.getY());
                            shapes.add(temp);
                            break;
                        }
                        case ELLIPSE: {
                            final Ellipse2D temp = new Ellipse2D.Double(point2D.getX(), point2D.getY(),
                                    w, h);
                            shapes.add(temp);
                            break;
                        }
                        case RECTANGLE: {
                            final Rectangle2D temp = new Rectangle2D.Double(point2D.getX(), point2D.getY(),
                                    w, h);
                            shapes.add(temp);
                            break;
                        }
                    }
                    panel.repaint();
                    points.clear();
                } else if (mode.equals(MOVE)) {
                    Point point = e.getPoint();
                    Point2D point2D = points.get(0);
                    points.clear();
                    if (selectedShape instanceof Line2D) {
                        shapes.remove(selectedShape);
                        Line2D line2D = (Line2D) MainWindow.this.selectedShape;
                        Double x1 = line2D.getX1() - point2D.getX() + point.getX();
                        Double y1 = line2D.getY1() - point2D.getY() + point.getY();
                        Double x2 = line2D.getX2() - point2D.getX() + point.getX();
                        Double y2 = line2D.getY2() - point2D.getY() + point.getY();

                        final Line2D temp = new Line2D.Double(x1, y1, x2, y2);
                        shapes.add(temp);
                        panel.repaint();
                        selectedShape = null;
                    }
                    if (selectedShape instanceof Ellipse2D) {
                        shapes.remove(selectedShape);
                        Ellipse2D ellipse2D = (Ellipse2D) selectedShape;
                        Double x1 = ellipse2D.getX() - point2D.getX() + point.getX();
                        Double y1 = ellipse2D.getY() - point2D.getY() + point.getY();
                        final Ellipse2D temp = new Ellipse2D.Double(x1, y1, ellipse2D.getWidth(), ellipse2D.getHeight());
                        shapes.add(temp);
                        panel.repaint();
                        selectedShape = null;
                    }
                    if (selectedShape instanceof Rectangle2D) {
                        shapes.remove(selectedShape);
                        Rectangle2D rectangle2D = (Rectangle2D) selectedShape;
                        Double x1 = rectangle2D.getX() - point2D.getX() + point.getX();
                        Double y1 = rectangle2D.getY() - point2D.getY() + point.getY();

                        final Rectangle2D temp = new Rectangle2D.Double(x1, y1, rectangle2D.getWidth(), rectangle2D.getHeight());
                        shapes.add(temp);
                        panel.repaint();
                        selectedShape = null;
                    }
                    clearInputs();
                } else if (mode.equals(RESIZE)) {
                    Point2D pointClicked = points.get(0);
                    Point2D pointReleased = e.getPoint();
                    log.info("Point clicked " + pointClicked + " Point released " + pointReleased);
                    points.clear();
                    if (selectedShape instanceof Line2D) {
                        log.info("Line selected");
                        Line2D line2D = (Line2D) MainWindow.this.selectedShape;
                        if (isLineApex(pointClicked, line2D)) {
                            shapes.remove(selectedShape);
                            log.info("Line apex clicked");
                            Double x1 = line2D.getX1();
                            Double y1 = line2D.getY1();
                            Double x2 = line2D.getX2();
                            Double y2 = line2D.getY2();
                            if (whichLineApex(pointClicked, line2D) == 0) {
                                log.info("Move first line point");
                                x1 = pointReleased.getX();
                                y1 = pointReleased.getY();
                            } else {
                                log.info("Move second line point");
                                x2 = pointReleased.getX();
                                y2 = pointReleased.getY();
                            }
                            final Line2D temp = new Line2D.Double(x1, y1, x2, y2);
                            shapes.add(temp);
                            panel.repaint();
                            selectedShape = null;

                        }
                    }
                    if (selectedShape instanceof Ellipse2D) {
                        Ellipse2D ellipse2D = (Ellipse2D) selectedShape;
                        if (ellipse2D.contains(pointClicked)) {
                            shapes.remove(selectedShape);
                            Double w, h;
                            if (pointClicked.getX() > pointReleased.getX()) {
                                w = ellipse2D.getWidth() - pointClicked.distance(pointReleased);
                                h = ellipse2D.getHeight() - pointClicked.distance(pointReleased);
                            } else {
                                w = ellipse2D.getWidth() + pointClicked.distance(pointReleased);
                                h = ellipse2D.getHeight() + pointClicked.distance(pointReleased);
                            }
                            final Ellipse2D temp = new Ellipse2D.Double(ellipse2D.getX(), ellipse2D.getY(), w, h);
                            shapes.add(temp);
                            panel.repaint();
                        }
                        selectedShape = null;
                    }
                    if (selectedShape instanceof Rectangle2D) {
                        Rectangle2D rectangle2D = (Rectangle2D) selectedShape;
                        if (rectangle2D.contains(pointClicked)) {
                            shapes.remove(selectedShape);
                            Double w, h;
                            if (pointClicked.getX() > pointReleased.getX()) {
                                w = rectangle2D.getWidth() - pointClicked.distance(pointReleased);
                                h = rectangle2D.getHeight() - pointClicked.distance(pointReleased);
                            } else {
                                w = rectangle2D.getWidth() + pointClicked.distance(pointReleased);
                                h = rectangle2D.getHeight() + pointClicked.distance(pointReleased);
                            }

                            final Rectangle2D temp = new Rectangle2D.Double(rectangle2D.getX(), rectangle2D.getY(), w, h);
                            shapes.add(temp);
                            panel.repaint();
                        }
                        selectedShape = null;
                    }
                    clearInputs();
                }
            }

        });

        JButton btnDraw = new JButton("Draw");
        btnDraw.addActionListener(e -> {
            String text = getSelectedButtonText(bg_1);
            log.info("Draw: " + text);
            String mode = getSelectedButtonText(bg_2);
            if (mode.equals(DRAW)) {
                if (checkIfEmptyInputs()) {
                    log.info("Empty input field");
                    return;
                } else if (text.equals(LINE)) {
                    final Line2D temp = new Line2D.Double(Double.parseDouble(textFieldX1.getText()), Double.parseDouble(textFieldY1.getText()),
                            Double.parseDouble(textFieldX2.getText()), Double.parseDouble(textFieldY2.getText()));
                    shapes.add(temp);
                    panel.repaint();
                } else if (text.equals(ELLIPSE)) {
                    final Ellipse2D temp = new Ellipse2D.Double(Double.parseDouble(textFieldX1.getText()), Double.parseDouble(textFieldY1.getText()),
                            Double.parseDouble(textFieldX2.getText()), Double.parseDouble(textFieldY2.getText()));
                    shapes.add(temp);
                    panel.repaint();
                } else if (text.equals(RECTANGLE)) {
                    final Rectangle2D temp = new Rectangle2D.Double(Double.parseDouble(textFieldX1.getText()), Double.parseDouble(textFieldY1.getText()),
                            Double.parseDouble(textFieldX2.getText()), Double.parseDouble(textFieldY2.getText()));
                    shapes.add(temp);
                    panel.repaint();
                }
                clearInputs();
            }
        });
        btnDraw.setBounds(936, 179, 114, 25);
        frame.getContentPane().add(btnDraw);

        textFieldX1 = new JTextField();
        textFieldX1.setBounds(936, 52, 48, 19);
        frame.getContentPane().add(textFieldX1);
        textFieldX1.setColumns(10);

        textFieldY1 = new JTextField();
        textFieldY1.setBounds(996, 52, 48, 19);
        frame.getContentPane().add(textFieldY1);
        textFieldY1.setColumns(10);

        textFieldX2 = new JTextField();
        textFieldX2.setColumns(10);
        textFieldX2.setBounds(936, 104, 48, 19);
        frame.getContentPane().add(textFieldX2);

        textFieldY2 = new JTextField();
        textFieldY2.setColumns(10);
        textFieldY2.setBounds(996, 104, 48, 19);
        frame.getContentPane().add(textFieldY2);

        JLabel lblX = new JLabel("x1");
        lblX.setBounds(939, 34, 21, 15);
        frame.getContentPane().add(lblX);

        JLabel label = new JLabel("y1");
        label.setBounds(998, 34, 21, 15);
        frame.getContentPane().add(label);

        JLabel label_1 = new JLabel("x2/w");
        label_1.setBounds(936, 83, 48, 15);
        frame.getContentPane().add(label_1);

        JLabel label_2 = new JLabel("y2/h");
        label_2.setBounds(998, 83, 42, 15);
        frame.getContentPane().add(label_2);

        JRadioButton rddraw = new JRadioButton(DRAW);
        bg_2.add(rddraw);
        rddraw.setSelected(true);
        rddraw.setBounds(518, 8, 144, 23);
        frame.getContentPane().add(rddraw);

        JRadioButton rdmove = new JRadioButton(MOVE);
        bg_2.add(rdmove);
        rdmove.setBounds(680, 8, 85, 23);
        frame.getContentPane().add(rdmove);

        JRadioButton rdresize = new JRadioButton(RESIZE);
        bg_2.add(rdresize);
        rdresize.setBounds(816, 8, 144, 23);
        frame.getContentPane().add(rdresize);

        JButton btnResize = new JButton("Resize");
        btnResize.addActionListener(e -> {
            String mode = getSelectedButtonText(bg_2);
            if (mode.equals(RESIZE)) {
                if (checkIfEmptyInputs()) {
                    log.info("Empty input field");
                    return;
                }
                if (selectedShape instanceof Line2D) {
                    shapes.remove(selectedShape);
                    final Line2D temp = new Line2D.Double(Double.parseDouble(textFieldX1.getText()), Double.parseDouble(textFieldY1.getText()),
                            Double.parseDouble(textFieldX2.getText()), Double.parseDouble(textFieldY2.getText()));
                    shapes.add(temp);
                    panel.repaint();
                }
                if (selectedShape instanceof Ellipse2D) {
                    shapes.remove(selectedShape);
                    final Ellipse2D temp = new Ellipse2D.Double(Double.parseDouble(textFieldX1.getText()), Double.parseDouble(textFieldY1.getText()),
                            Double.parseDouble(textFieldX2.getText()), Double.parseDouble(textFieldY2.getText()));
                    shapes.add(temp);
                    panel.repaint();
                }
                if (selectedShape instanceof Rectangle2D) {
                    shapes.remove(selectedShape);
                    final Rectangle2D temp = new Rectangle2D.Double(Double.parseDouble(textFieldX1.getText()), Double.parseDouble(textFieldY1.getText()),
                            Double.parseDouble(textFieldX2.getText()), Double.parseDouble(textFieldY2.getText()));
                    shapes.add(temp);
                    panel.repaint();
                }
                selectedShape = null;
                clearInputs();
            }
        });
        btnResize.setBounds(936, 227, 114, 25);
        frame.getContentPane().add(btnResize);

        JButton btnClear = new JButton("Clear");
        btnClear.addActionListener(e -> {
            shapes.clear();
            panel.repaint();
        });
        btnClear.setBounds(936, 350, 114, 25);
        frame.getContentPane().add(btnClear);
    }

    private boolean isLineApex(Point2D point, Line2D line2D) {
        return point.distance(line2D.getP1()) < 10 || point.distance(line2D.getP2()) < 10;
    }

    private int whichLineApex(Point2D point, Line2D line2D) {
        if (point.distance(line2D.getP1()) < 10)
            return 0;
        else
            return 1;
    }

    private void selectShape(Point point) {
        for (Shape shape : shapes) {
            if (shape instanceof Line2D) {
                Line2D tempLine = (Line2D) shape;
                if (tempLine.ptLineDist(point) < 1) {
                    selectedShape = shape;
                    textFieldX1.setText(String.valueOf(((Line2D) selectedShape).getX1()));
                    textFieldY1.setText(String.valueOf(((Line2D) selectedShape).getY1()));
                    textFieldX2.setText(String.valueOf(((Line2D) selectedShape).getX2()));
                    textFieldY2.setText(String.valueOf(((Line2D) selectedShape).getY2()));
                }
            } else if (shape.contains(point)) {
                selectedShape = shape;
                if (selectedShape instanceof Rectangle2D) {
                    textFieldX1.setText(String.valueOf(((Rectangle2D) selectedShape).getX()));
                    textFieldY1.setText(String.valueOf(((Rectangle2D) selectedShape).getY()));
                    textFieldX2.setText(String.valueOf(((Rectangle2D) selectedShape).getWidth()));
                    textFieldY2.setText(String.valueOf(((Rectangle2D) selectedShape).getHeight()));
                } else if (selectedShape instanceof Ellipse2D) {
                    textFieldX1.setText(String.valueOf(((Ellipse2D) selectedShape).getX()));
                    textFieldY1.setText(String.valueOf(((Ellipse2D) selectedShape).getY()));
                    textFieldX2.setText(String.valueOf(((Ellipse2D) selectedShape).getWidth()));
                    textFieldY2.setText(String.valueOf(((Ellipse2D) selectedShape).getHeight()));
                }
                break;
            }
        }
    }

    private void clearInputs() {
        textFieldX1.setText("");
        textFieldY1.setText("");
        textFieldX2.setText("");
        textFieldY2.setText("");
    }

    private boolean checkIfEmptyInputs() {
        return textFieldX1.getText().isEmpty() || textFieldY1.getText().isEmpty() || textFieldX2.getText().isEmpty() || textFieldY2.getText().isEmpty();

    }

    private String getSelectedButtonText(ButtonGroup buttonGroup) {
        for (Enumeration<AbstractButton> buttons = buttonGroup.getElements(); buttons.hasMoreElements(); ) {
            AbstractButton button = buttons.nextElement();

            if (button.isSelected()) {
                return button.getText();
            }
        }

        return null;
    }
}
