package gui;

import com.jogamp.opengl.*;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.FPSAnimator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public abstract class CubeRoot implements GLEventListener {
    protected GL2 g;

    private static GLProfile profile;
    private static GLCapabilities capabilities;

    private FPSAnimator fpsAnimator;
    private GLCanvas glCanvas;

    private JFrame frame;
    private JPanel panel;

    private static InputMap inputMap;
    private static ActionMap actionMap;

    private static final GLU glu = new GLU();

    public abstract void registerAllKeyActions();
    public abstract void display(GLAutoDrawable glAutoDrawable);

    protected void drawFigureQuads3f(float [][] figure){
        if(figure==null | figure[0].length!=3)
            return;
        g.glBegin(GL2.GL_QUADS);
        for (float[] floats : figure) {
            if (floats[0] == -1f &&  floats[1]==-1f && floats[2] ==-1f){
                g.glColor3f(0,0,0);
            }
            if (floats[0] == -1f &&  floats[1]==-1f && floats[2] ==1f){
                g.glColor3f(0,0,255);
            }
            if (floats[0] == -1f &&  floats[1]==1f && floats[2] ==-1f){
                g.glColor3f(0,255,0);
            }
            if (floats[0] == 1f &&  floats[1]==-1f && floats[2] ==-1f){
                g.glColor3f(255,0,0);
            }
            if (floats[0] == -1f &&  floats[1]==1 && floats[2] ==1f){
                g.glColor3f(0,255,255);
            }
            if (floats[0] == 1f &&  floats[1]==-1f && floats[2] ==1f){
                g.glColor3f(255,0,255);
            }
            if (floats[0] == 1f &&  floats[1]==1f && floats[2] ==-1f){
                g.glColor3f(255,255,0);
            }
            if (floats[0] == 1f &&  floats[1]==1f && floats[2] ==1f){
                g.glColor3f(255,255,255);
            }
            g.glVertex3f(floats[0], floats[1], floats[2]);
        }
        g.glEnd();
    }

    private void initGLObjects() {
        profile = GLProfile.get(GLProfile.GL2);
        capabilities = new GLCapabilities(profile);
    }

    private void createWindow() {
        glCanvas = new GLCanvas(capabilities);
        glCanvas.addGLEventListener(this);
        glCanvas.setSize(800, 600);

        frame = new JFrame();
        frame.getContentPane().add(glCanvas);
        frame.setSize(frame.getContentPane().getPreferredSize());
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        int x = (d.width - frame.getWidth()) / 2;
        int y = (d.height - frame.getHeight()) / 2;

        frame.setLocation(x, y);

        panel = new JPanel();
        panel.setPreferredSize(new Dimension(0, 0));
        frame.add(panel);

        actionMap = panel.getActionMap();
        inputMap = panel.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);


        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (fpsAnimator.isStarted()) {
                    fpsAnimator.stop();
                    System.exit(0);
                }
            }
        });

    }

    public void registerKeyAction(Integer key, AbstractAction a) {
        inputMap.put(KeyStroke.getKeyStroke(key, 0), key.toString());
        actionMap.put(key.toString(), a);

    }


    public void start() {
        fpsAnimator.start();
    }

    public void clearCanvas() {
        g.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        g.glLoadIdentity();
    }

    public void init(GLAutoDrawable d) {
        g = d.getGL().getGL2();
        g.glShadeModel(GL2.GL_SMOOTH);
        g.glClearColor(0, 0, 0, 0);
        g.glClearDepth(1);
        g.glEnable(GL2.GL_DEPTH_TEST);
        g.glDepthFunc(GL2.GL_LEQUAL);
        g.glHint(GL2.GL_PERSPECTIVE_CORRECTION_HINT, GL2.GL_NICEST);
    }

    public void reshape(GLAutoDrawable d, int x, int y, int width, int height) {
        g.glViewport(0, 0, width, height);
        g.glMatrixMode(GL2.GL_PROJECTION);
        g.glLoadIdentity();
        glu.gluPerspective(45f, (float) width/ (float) height, 1, 20);
        g.glMatrixMode(GL2.GL_MODELVIEW);
        g.glLoadIdentity();
    }

    public void dispose(GLAutoDrawable glAutoDrawable) {

    }


    public CubeRoot(){
        initGLObjects();
        createWindow();
        registerAllKeyActions();
        fpsAnimator = new FPSAnimator(glCanvas,200,true);
        frame.setVisible(true);
    }
}
