package gui;

import com.jogamp.opengl.GLAutoDrawable;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class Cube extends CubeRoot {
    private float xrotation;
    private float yrotation;
    private float zrotation;
    private float[][] figure= {
            //front
            {-1,-1,1},
            {1,-1,1},
            {1,1,1},
            {-1,1,1},

            //back
            {-1,1,-1},
            {1,1,-1},
            {1,-1,-1},
            {-1,-1,-1},

            //top
            {-1,1,1},
            {1,1,1},
            {1,1,-1},
            {-1,1,-1},

            //bottom
            {-1,-1,1},
            {-1,-1,-1},
            {1,-1,-1},
            {1,-1,1},

            //left
            {-1,-1,1},
            {-1,1,1},
            {-1,1,-1},
            {-1,-1,-1},

            //right
            {1,1,-1},
            {1,1,1},
            {1,-1,1},
            {1,-1,-1}


    };

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        clearCanvas();
        g.glTranslatef(0,0,-6.6f);
        g.glRotatef(xrotation,1,0,0);
        g.glRotatef(yrotation,0,1,0);
        g.glRotatef(zrotation,0,0,1);

        drawFigureQuads3f(figure);
        g.glFlush();
    }

    @Override
    public void registerAllKeyActions() {
        registerKeyAction(KeyEvent.VK_UP, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                 xrotation += 3;
            }
        });

        registerKeyAction(KeyEvent.VK_DOWN, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                xrotation -= 3;
            }
        });

        registerKeyAction(KeyEvent.VK_LEFT, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                yrotation += 3;
            }
        });

        registerKeyAction(KeyEvent.VK_RIGHT, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                yrotation -= 3;
            }
        });
    }



    public static void main(String[] args) {
        new Cube().start();
    }

}