package gui;

import lombok.Data;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;

@Data
public class ImagePanel extends JPanel {
    private BufferedImage bufferedImage;

    public ImagePanel(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    public BufferedImage getBufferedImage() {
        return bufferedImage;
    }

    public void setBufferedImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(bufferedImage==null)
            return;
        Graphics2D g2d = (Graphics2D) g;
        if(bufferedImage.getHeight()<20 || bufferedImage.getWidth()<20){
            g2d.scale(bufferedImage.getHeight()*10,bufferedImage.getWidth()*10);
        }
        g2d.drawImage(bufferedImage, 0, 0, null);
    }
}
