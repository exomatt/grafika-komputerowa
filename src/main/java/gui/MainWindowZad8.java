package gui;

import imageutils.ImageReaderSaver;
import imageutils.MorphologicalFiltration;
import imageutils.PercentageOfColour;
import imageutils.binarization.BinarizationOperations;
import lombok.extern.java.Log;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.Arrays;

import static imageutils.filtration.Filtration.copyImage;

@Log
public class MainWindowZad8 {

    private JFrame frame;
    private BufferedImage image;
    private BufferedImage firstImage;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(() -> {
            try {
                MainWindowZad8 window = new MainWindowZad8();
                window.frame.setVisible(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * Create the application.
     *
     * @wbp.parser.entryPoint
     */
    public MainWindowZad8() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1284, 863);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);
        JMenu mnFiles = new JMenu("Files");
        menuBar.add(mnFiles);
        JMenuItem mntmLoad = new JMenuItem("Load");
        mnFiles.add(mntmLoad);
        ImagePanel imagePanel = new ImagePanel(image);
        frame.getContentPane().add(imagePanel);
        mntmLoad.addActionListener(e -> {
            JFileChooser imageOpener = new JFileChooser("/home/exomat/Pulpit/grafika/ppm-obrazy-testowe");
            imageOpener.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    String fileName = f.getName().toLowerCase();
                    return fileName.endsWith(".jpg") || fileName.endsWith(".ppm") || fileName.endsWith(".jpeg") || fileName.endsWith(".png");
                }

                @Override
                public String getDescription() {
                    return "Image files (.ppm, .jpeg, .jpg, .png)";
                }
            });

            int returnValue = imageOpener.showDialog(null, "Select image");
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                try {
                    image = ImageReaderSaver.loadImage(imageOpener.getSelectedFile().getPath());
                    firstImage = ImageReaderSaver.loadImage(imageOpener.getSelectedFile().getPath());
                } catch (Exception ex) {
//                    log.severe("Problem with reading image" + ex.getMessage() + Arrays.toString(ex.getStackTrace()));
                    JOptionPane.showMessageDialog(null, "Problem with reading file!!", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
                imagePanel.setBufferedImage(image);
                frame.setPreferredSize(new Dimension(image.getWidth(), image.getHeight()));
                imagePanel.repaint();

            }

        });

        JMenuItem mntmSave = new JMenuItem("Save");
        mnFiles.add(mntmSave);

        JMenu mnMorphological = new JMenu("Morphological");
        menuBar.add(mnMorphological);

        JMenuItem mntmDilatation = new JMenuItem("Dilatation");
        mntmDilatation.addActionListener(e -> {
            image = MorphologicalFiltration.filterDilatation(image);
            imagePanel.setBufferedImage(image);
            imagePanel.repaint();
        });
        mnMorphological.add(mntmDilatation);

        JMenuItem mntmErosion = new JMenuItem("Erosion");
        mntmErosion.addActionListener(e -> {
            image = MorphologicalFiltration.filterErosion(image);
            imagePanel.setBufferedImage(image);
            imagePanel.repaint();
        });
        mnMorphological.add(mntmErosion);

        JMenuItem mntmOpening = new JMenuItem("Opening");
        mntmOpening.addActionListener(e -> {
            image = MorphologicalFiltration.filterOpening(image);
            imagePanel.setBufferedImage(image);
            imagePanel.repaint();
        });
        mnMorphological.add(mntmOpening);

        JMenuItem mntmClosure = new JMenuItem("Closure");
        mntmClosure.addActionListener(e -> {
            image = MorphologicalFiltration.filterClosing(image);
            imagePanel.setBufferedImage(image);
            imagePanel.repaint();
        });
        mnMorphological.add(mntmClosure);

        JMenuItem mntmHitormissT = new JMenuItem("Hit-or-miss Thinning");
        mntmHitormissT.addActionListener(e -> {
            image = MorphologicalFiltration.hitOrMiss(image, true);
            imagePanel.setBufferedImage(image);
            imagePanel.repaint();
        });
        mnMorphological.add(mntmHitormissT);

        JMenuItem mntmHitormissThickening = new JMenuItem("Hit-or-miss  Thickening");
        mntmHitormissThickening.addActionListener(e -> {
            image = MorphologicalFiltration.hitOrMiss(image, false);
            imagePanel.setBufferedImage(image);
            imagePanel.repaint();
        });
        mnMorphological.add(mntmHitormissThickening);

        JMenu mnClear = new JMenu("Clear");
        menuBar.add(mnClear);

        JMenuItem mntmShowOriginalPhoto = new JMenuItem("Show original photo");
        mntmShowOriginalPhoto.addActionListener(e -> {
            image = copyImage(firstImage);
            imagePanel.setBufferedImage(image);
            imagePanel.repaint();
        });
        mnClear.add(mntmShowOriginalPhoto);

        JMenu mnBinarization = new JMenu("Binarization");
        menuBar.add(mnBinarization);

        JMenuItem mntmBernsen = new JMenuItem("bernsen ");
        mntmBernsen.addActionListener(e -> {
            image = new BinarizationOperations().userValueBinarization(image, 150);
            imagePanel.setBufferedImage(image);
            imagePanel.repaint();
        });
        mnBinarization.add(mntmBernsen);

        JMenu mnPercentageOfColour = new JMenu("Percentage of colour ");
        menuBar.add(mnPercentageOfColour);

        JMenuItem mntmGreen = new JMenuItem("All colors");
        mntmGreen.addActionListener(e -> {
            DecimalFormat df = new DecimalFormat("####0.00");
            //        0- red, 1-yellow, 2-green, 3-cyan, 4-blue, 5-magenta, 6-nearly white, 7-nearly black
            int[] colors = PercentageOfColour.percentage(image);
            int allColors = image.getHeight() * image.getWidth();
            double red = ((double) colors[0] / allColors) * 100;
            double yellow = ((double) colors[1] / allColors) * 100;
            double green = ((double) colors[2] / allColors) * 100;
            double cyan = ((double) colors[3] / allColors) * 100;
            double blue = ((double) colors[4] / allColors) * 100;
            double magenta = ((double) colors[5] / allColors) * 100;
            double white = ((double) colors[6] / allColors) * 100;
            double black = ((double) colors[7] / allColors) * 100;

            String stringBuilder = "Percentage of colors: \n" +
                    "red: " + df.format(red) + "% " + colors[0] + "/" + allColors + "\n" +
                    "yellow: " + df.format(yellow) + "% " + colors[1] + "/" + allColors + "\n" +
                    "green: " + df.format(green) + "% " + colors[2] + "/" + allColors + "\n" +
                    "cyan: " + df.format(cyan) + "% " + colors[3] + "/" + allColors + "\n" +
                    "blue: " + df.format(blue) + "% " + colors[4] + "/" + allColors + "\n" +
                    "magenta: " + df.format(magenta) + "% " + colors[5] + "/" + allColors + "\n" +
                    "nearly white or white: " + df.format(white) + "% " + colors[6] + "/" + allColors + "\n" +
                    "nearly black or black: " + df.format(black) + "% " + colors[7] + "/" + allColors + "\n";
            JOptionPane.showMessageDialog(null, stringBuilder, "Percentage",
                    JOptionPane.INFORMATION_MESSAGE);
        });
        mnPercentageOfColour.add(mntmGreen);


        mntmSave.addActionListener(e -> {
            JFileChooser imageOpener = new JFileChooser("/home/exomat/Pulpit/grafika/ppm-obrazy-testowe");
            imageOpener.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            imageOpener.setFileFilter(new FileFilter() {
                @Override
                public boolean accept(File f) {
                    String fileName = f.getName().toLowerCase();
                    return fileName.endsWith(".jpg") || fileName.endsWith(".jpeg") || fileName.endsWith(".png");
                }

                @Override
                public String getDescription() {
                    return "Image files (.jpg, .jpeg, .png)";
                }
            });
            int returnValue = imageOpener.showDialog(null, "Save image");
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                String path = imageOpener.getSelectedFile().getPath();
                String inputDialog = JOptionPane
                        .showInputDialog("Input proccent of compression (0 maximum, 100 none )");
                int parseInt;
                try {
                    parseInt = Integer.parseInt(inputDialog);
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, "Please fill with number!!", "Error",
                            JOptionPane.ERROR_MESSAGE);
                    return;
                }
                Float quality = (float) parseInt / 100;
                try {
                    ImageReaderSaver.saveImage(image, path, quality);
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
