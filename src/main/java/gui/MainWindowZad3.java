package gui;

import com.jogamp.opengl.util.FPSAnimator;

import java.awt.EventQueue;


import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class MainWindowZad3 {

    private JFrame frame;
    private JTextField textFieldR;
    private JTextField textFieldG;
    private JTextField textFieldB;
    private JTextField textFieldC;
    private JTextField textFieldY;
    private JTextField textFieldM;
    private JTextField textFieldK;
    private JTextField textFieldColorDisplay;
    private JSlider sliderR;
    private JSlider sliderG;
    private JSlider sliderB;
    private JSlider sliderC;
    private JSlider sliderM;
    private JSlider sliderY;
    private JSlider sliderK;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    MainWindowZad3 window = new MainWindowZad3();
                    window.frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the application.
     */
    public MainWindowZad3() {
        initialize();
    }

    /**
     * Initialize the contents of the frame.
     */
    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 1074, 548);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JLabel lblRgb = new JLabel("RGB");
        lblRgb.setBounds(835, 12, 66, 15);
        frame.getContentPane().add(lblRgb);

        sliderR = new JSlider(0, 255, 0);
        sliderR.setMajorTickSpacing(50);
        sliderR.setMinorTickSpacing(5);
        sliderR.setPaintTrack(true);
        sliderR.setPaintTicks(true);
        sliderR.setPaintLabels(true);
        sliderR.setBounds(824, 52, 200, 16);
        sliderR.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                textFieldR.setText(String.valueOf(value));
                RGBToCMYK();
            }
        });
        frame.getContentPane().add(sliderR);

        sliderG = new JSlider(0, 255, 0);
        sliderG.setMajorTickSpacing(50);
        sliderG.setMinorTickSpacing(5);
        sliderG.setPaintTrack(true);
        sliderG.setPaintTicks(true);
        sliderG.setPaintLabels(true);
        sliderG.setBounds(824, 80, 200, 16);
        sliderG.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                textFieldG.setText(String.valueOf(value));
                RGBToCMYK();
            }
        });
        frame.getContentPane().add(sliderG);

        sliderB = new JSlider(0, 255, 0);
        sliderB.setMajorTickSpacing(50);
        sliderB.setMinorTickSpacing(5);
        sliderB.setPaintTrack(true);
        sliderB.setPaintTicks(true);
        sliderB.setPaintLabels(true);
        sliderB.setBounds(824, 118, 200, 16);
        sliderB.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                textFieldB.setText(String.valueOf(value));
                RGBToCMYK();
            }
        });
        frame.getContentPane().add(sliderB);

        textFieldR = new JTextField();
        textFieldR.setBounds(757, 49, 66, 19);
        textFieldR.setColumns(10);
        textFieldR.setText("0");
        frame.getContentPane().add(textFieldR);

        textFieldG = new JTextField();
        textFieldG.setText("0");
        textFieldG.setColumns(10);
        textFieldG.setBounds(757, 80, 66, 19);
        frame.getContentPane().add(textFieldG);

        textFieldB = new JTextField();
        textFieldB.setText("0");
        textFieldB.setColumns(10);
        textFieldB.setBounds(757, 115, 66, 19);
        frame.getContentPane().add(textFieldB);

        JLabel lblCmyk = new JLabel("CMYK");
        lblCmyk.setBounds(835, 175, 66, 15);
        frame.getContentPane().add(lblCmyk);

        sliderC = new JSlider(0, 100, 0);
        sliderC.setPaintTrack(true);
        sliderC.setPaintTicks(true);
        sliderC.setPaintLabels(true);
        sliderC.setMinorTickSpacing(5);
        sliderC.setMajorTickSpacing(50);
        sliderC.setBounds(824, 203, 200, 16);
        sliderC.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                textFieldC.setText(String.valueOf(value));
                CMYKToRGB();
            }
        });
        frame.getContentPane().add(sliderC);

        sliderY = new JSlider(0, 100, 0);
        sliderY.setPaintTrack(true);
        sliderY.setPaintTicks(true);
        sliderY.setPaintLabels(true);
        sliderY.setMinorTickSpacing(5);
        sliderY.setMajorTickSpacing(50);
        sliderY.setBounds(824, 269, 200, 16);
        sliderY.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                textFieldY.setText(String.valueOf(value));
                CMYKToRGB();

            }
        });
        frame.getContentPane().add(sliderY);

        sliderM = new JSlider(0, 100, 0);
        sliderM.setPaintTrack(true);
        sliderM.setPaintTicks(true);
        sliderM.setPaintLabels(true);
        sliderM.setMinorTickSpacing(5);
        sliderM.setMajorTickSpacing(50);
        sliderM.setBounds(824, 231, 200, 16);
        sliderM.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                textFieldM.setText(String.valueOf(value));
                CMYKToRGB();
            }
        });
        frame.getContentPane().add(sliderM);

        sliderK = new JSlider(0, 100, 0);
        sliderK.setPaintTrack(true);
        sliderK.setPaintTicks(true);
        sliderK.setPaintLabels(true);
        sliderK.setMinorTickSpacing(5);
        sliderK.setMajorTickSpacing(50);
        sliderK.setBounds(824, 304, 200, 16);
        sliderK.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int value = ((JSlider) e.getSource()).getValue();
                textFieldK.setText(String.valueOf(value));
                CMYKToRGB();
            }
        });
        frame.getContentPane().add(sliderK);
        textFieldM = new JTextField();
        textFieldM.setText("0");
        textFieldM.setColumns(10);
        textFieldM.setBounds(757, 231, 66, 19);
        frame.getContentPane().add(textFieldM);

        textFieldY = new JTextField();
        textFieldY.setColumns(10);
        textFieldY.setText("0");
        textFieldY.setBounds(757, 266, 66, 19);
        frame.getContentPane().add(textFieldY);

        textFieldC = new JTextField();
        textFieldC.setColumns(10);
        textFieldC.setText("0");
        textFieldC.setBounds(757, 200, 66, 19);
        frame.getContentPane().add(textFieldC);


        textFieldK = new JTextField();
        textFieldK.setColumns(10);
        textFieldK.setText("0");
        textFieldK.setBounds(757, 301, 66, 19);
        frame.getContentPane().add(textFieldK);

        textFieldColorDisplay = new JTextField();
        textFieldColorDisplay.setForeground(new Color(255, 255, 255));
        textFieldColorDisplay.setBounds(849, 409, 124, 19);
        frame.getContentPane().add(textFieldColorDisplay);
        textFieldColorDisplay.setColumns(10);

        JPanel panel = new JPanel();
        panel.setBounds(12, 17, 506, 411);
        frame.getContentPane().add(panel);
        
        JButton btnCalculateCmyk = new JButton("calculate CMYK ");
        btnCalculateCmyk.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        	    RGBToCMYK();
        	}
        });
        btnCalculateCmyk.setBounds(589, 89, 156, 25);
        frame.getContentPane().add(btnCalculateCmyk);
        
        JButton btnCalculateRgb = new JButton("calculate RGB ");
        btnCalculateRgb.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
                CMYKToRGB();
        	}
        });
        btnCalculateRgb.setBounds(589, 240, 156, 25);
        frame.getContentPane().add(btnCalculateRgb);

        JMenuBar menuBar = new JMenuBar();
        frame.setJMenuBar(menuBar);

        JMenu mnNewMenu = new JMenu("option");
        menuBar.add(mnNewMenu);

        JMenuItem mntmColorChooser = new JMenuItem("RGB cube ");
        mnNewMenu.add(mntmColorChooser);
        mntmColorChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               //todo tutaj kostke odpalic
                new Cube().start();
            }
        });
    }

    private void CMYKToRGB() {
        List<Integer> list = calculateColorsCMYKRGB();
        textFieldR.setText(String.valueOf(list.get(0)));
        textFieldG.setText(String.valueOf(list.get(1)));
        textFieldB.setText(String.valueOf(list.get(2)));
//        sliderR.setValue(list.get(0));
//        sliderG.setValue(list.get(1));
//        sliderB.setValue(list.get(2));
        textFieldColorDisplay.setForeground(new Color(list.get(0), list.get(1), list.get(2)));
        textFieldColorDisplay.setBackground(new Color(list.get(0), list.get(1), list.get(2)));
    }

    private void RGBToCMYK() {
        List<Integer> list = calculateColorsRGBCMYK();
        textFieldC.setText(String.valueOf(list.get(0)));
        textFieldM.setText(String.valueOf(list.get(1)));
        textFieldY.setText(String.valueOf(list.get(2)));
        textFieldK.setText(String.valueOf(list.get(3)));
        list =  calculateColorsCMYKRGB();
        textFieldColorDisplay.setForeground(new Color(list.get(0), list.get(1), list.get(2)));
        textFieldColorDisplay.setBackground(new Color(list.get(0), list.get(1), list.get(2)));
    }

    private List<Integer> calculateColorsCMYKRGB() {
        List<Integer> list = new ArrayList<>();
        double cyan = Double.parseDouble(textFieldC.getText()) / 100;
        double yellow = Double.parseDouble(textFieldY.getText()) / 100;
        double magenta = Double.parseDouble(textFieldM.getText()) / 100;
        double black = Double.parseDouble(textFieldK.getText()) / 100;
        double red = 255 * (1 - Double.min(1, (cyan * (1 - black)) + black));
        double green = 255 * (1 - Double.min(1, (magenta * (1 - black)) + black));
        double blue = 255 * (1 - Double.min(1, (yellow * (1 - black)) + black));
        list.add((int) Math.round(red));
        list.add((int) Math.round(green));
        list.add((int) Math.round(blue));
        return list;
    }

    private List<Integer> calculateColorsRGBCMYK() {
        List<Integer> list = new ArrayList<>();
        double red = Integer.parseInt(textFieldR.getText())/(double)255;
        double green = Integer.parseInt(textFieldG.getText())/(double)255;
        double blue = Integer.parseInt(textFieldB.getText())/(double)255;
        double black = 1-Double.max(Double.max(red,green), blue);
        double temp = black==0?1:1-black;
        double cyan = (1 - red - black) / temp;
        double magenta = (1 - green - black) / temp;
        double yellow = (1 - blue - black) / temp;
        list.add((int) Math.round(cyan*100));
        list.add((int) Math.round(magenta*100));
        list.add((int) Math.round(yellow*100));
        list.add((int) Math.round(black*100));
        return list;
    }
}
